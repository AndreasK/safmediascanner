/*
 * Copyright (C) 2018-19 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <string.h>
#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>

#include "native-lib-utils.h"

#define MEGABYTE (1024 * 1024)
static const char * sVarious = "≠";


/************************************************************************************
 *
 * safe string copy with automatic truncation
 *
 ***********************************************************************************/
void strCpySafe(char *dst, const char *src)
{
    if (src == nullptr)
    {
        dst[0] = '\0';
    }
    else
    {
        strncpy(dst, src, MAXSTR - 1);
        dst[MAXSTR - 1] = '\0';
    }
}


#if 0
/************************************************************************************
 *
 * safe string compare
 *
 ***********************************************************************************/
extern int strCmpSafe(const char *s1, const char *s2)
{
    if (strZeroOrEmpty(s1))
    {
        if (strZeroOrEmpty(s2))
            return 0;
        else
            return -1;
    }
    else
    if (strZeroOrEmpty(s2))
        return 1;
    else
        return strcmp(s1, s2);
}
#endif


/************************************************************************************
 *
 * copy unique string
 *
 ***********************************************************************************/
void strCpyUnique(char *dst, const char *src)
{
    if (dst[0] != sVarious[0])
    {
        if (dst[0] == '\0')
        {
            strCpySafe(dst, src);   // not set before. Now dst may also be empty.
        }
        else
        if ((src == nullptr) || strcmp(src, dst))
        {
            //LOGW("strCpyUnique(dst: %s, src: %s\n", dst, src);
            strcpy(dst, sVarious);  // values differ
        }
    }
}


/************************************************************************************
 *
 * make a copy of the strings for later use
 *
 ***********************************************************************************/
const char *copyNonEmptyString(const char *s, const char *dotpos)
{
    if (s == nullptr)
    {
        return nullptr;
    }
    const char *t = s;
    SKIP_SPACE(t);
    if (*t == '\0')
    {
        return nullptr;
    }

    size_t l;
    if (dotpos != nullptr)
    {
        l = (dotpos >= t) ? (dotpos - t) : (0);
    }
    else
    {
        l = strlen(t);
    }

    char *sn = (char *) malloc(l + 1);   // including '\0'
    memcpy(sn, t, l);
    sn[l] = '\0';   // end with EOS
    return sn;
}


/************************************************************************************
 *
 * helper to get int from long
 *
 ***********************************************************************************/
int fromLong(long l)
{
    if ((l < 0) || (l > INT32_MAX))
    {
        return 0;
    }
    else
    {
        return (int) l;
    }
}


/************************************************************************************
 *
 * helper to evaluate strings like "n/m" or "n"
 *
 * return 0 if no error
 ***********************************************************************************/
int evalNfromM(int *n, int *m, const char *s)
{
    if (s == nullptr)
    {
        // no string
        *n = *m = 0;
        return 0;
    }
    char *endptr;
    long l;
    l = strtol(s, &endptr, 10);
    if (endptr == s)
    {
        // malformed string
        *n = *m = 0;        // both N and M invalid
        return -1;
    }
    *n = fromLong(l);
    SKIP_SPACE(endptr)
    if (*endptr == '\0')
    {
        *m = 0;     // no M
        return 0;
    }
    if (*endptr != '/')
    {
        *m = 0;     // M invalid
        return -2;
    }
    endptr++;
    SKIP_SPACE(endptr)
    l = strtol(s, &endptr, 10);
    if (endptr == s)
    {
        // malformed M
        *m = 0;        // M invalid
        return -3;
    }
    *m = fromLong(l);
    return 0;
}


/******************************************************************************
 *
 * helper to get a file name from a path
 *
 *****************************************************************************/
const char * getFileNameFromPath(const char *path)
{
    const char *s = strrchr(path, '/');
    if (s == nullptr)
    {
        return path;
    }

    return s + 1;
}


/******************************************************************************
 *
 * helper to get a directory name
 *
 * /a/bb/ccc -> ccc
 * /a/bb/ccc/ -> ccc
 * /a/bb/ccc/// -> ccc
 * / -> /
 *
 *****************************************************************************/
void getDirNameFromPath(const char *path, long l, int *start_index, int *stop_index)
{
    if (l <= 0)
    {
        *start_index = *stop_index = 0;
        return;
    }

    // skip trailing '/'
    const char *se = path + l - 1;
    while ((se > path) && (*se == '/'))
    {
        se--;
    }
    const char *ss = se;
    do
    {
        ss--;
    }
    while((ss > path) && (*ss != '/'));

    if (*ss == '/')
    {
        ss++;
    }
    *stop_index = (int) (se - path + 1);
    *start_index = (int) (ss - path);
}


/******************************************************************************
 *
 * helper to get a directory name
 *
 *****************************************************************************/
void extractDirNameFromPath
(
    const char *path,       // src
    long lp,                // length of path
    char *fname,            // dst
    const char **startp     // out: pointer to fname inside path
)
{
    int n, m;
    getDirNameFromPath(path, lp, &n, &m);
    assert(m >= n);
    size_t l = (size_t) (m - n);
    if (l > MAXFNAME)
    {
        l = MAXFNAME;
    }
    strncpy(fname, path + n, (size_t) (m - n));
    fname[l] = '\0';
    *startp = path + n;
}


/******************************************************************************
 *
 * helper to check if directory name is CD<n>
 *
 * returns <n> or -1 in case of no match
 *
 *****************************************************************************/
int checkDirNameIsCdNumber(const char *dirname, int l)
{
    static const char *prefix = "CD";
    static const int prefixlen = 2;

    if ((l > prefixlen) && !strncasecmp(dirname, prefix, strlen(prefix)))
    {
        dirname += prefixlen;
        l -= prefixlen;
        char *endptr;
        long n = strtol(dirname, &endptr, 10); // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
        if ((n > 0) && (endptr > dirname) && (endptr - dirname == l))
        {
            return (int) n;     // ignore overflow here
        }
    }

    return -1;
}


/******************************************************************************
 *
 * completely read a non-seekable file into memory
 *
 * len is is input and output parameter, currently input is ignored
 * returns 0 and a buffer pointer or an error code
 *
 *****************************************************************************/
int readFileOfUnknownLength(int fd, char **pbuf, long *len)
{
    // start with 3 MB, then increment by 1 MB until a maximum of
    int result = ENOMEM;
    const size_t mem_start = 3 * MEGABYTE;
    const size_t mem_increment = MEGABYTE;
    const size_t mem_max = 100 * MEGABYTE;

    char *buf = nullptr;
    size_t curr_file_size = 0;
    size_t curr_buf_incr = mem_start;
    while((buf = (char *) realloc(buf, curr_file_size + curr_buf_incr)) != nullptr)
    {
        ssize_t res = read(fd, buf + curr_file_size, curr_buf_incr);
        if (res < 0)
        {
            // read error
            result = errno;
            break;
        }
        // According to the documentation it might happen that we got less bytes than requested,
        // but before reaching EOF, so better explicitly only detect 0 for EOF
        if (res == 0)
        {
            // end-of-file: shrink memory block end return pointer
            buf = (char *) realloc(buf, curr_file_size);
            if (buf != nullptr)
            {
                result = 0;
            }
            else
            {
                // should not happen, return ENOMEM
            }
            break;
        }

        curr_file_size += res;
        curr_buf_incr = mem_increment;
        if (curr_file_size >= mem_max)
        {
            // this file is too large, return ENOMEM
            break;
        }
    }

    // in case of failure: free memory and return nullptr
    if (result)
    {
        free(buf);
        buf = nullptr;
        curr_file_size = 0;
    }

    *len = (long) curr_file_size;
    *pbuf = buf;
    return result;
}
