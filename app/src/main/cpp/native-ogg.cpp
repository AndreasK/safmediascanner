/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>

#include "native-lib.h"
#include "native-ogg.h"
#include "native-cmd.h"

// advanced taglib interface
#include <ogg/vorbis/vorbisfile.h>
#include <ogg/opus/opusfile.h>

#include "native-lib-utils.h"
#include "native-xiph.h"
#include "native-flac.h"


/************************************************************************************
 *
 * advanced ogg vorbis handling
 *
 ***********************************************************************************/
void handleOggVorbis(audioFileInfo *info, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::Ogg::Vorbis::File f(stream);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    TagLib::Ogg::XiphComment *tag = f.tag();
    if (tag != nullptr)
    {
        handleOgg(info, tag);
        info->tag_type = eTagOgg;
    }
}


/************************************************************************************
 *
 * advanced ogg opus handling
 *
 ***********************************************************************************/
void handleOggOpus(audioFileInfo *info, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::Ogg::Opus::File f(stream);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    TagLib::Ogg::XiphComment *tag = f.tag();
    if (tag != nullptr)
    {
        handleOgg(info, tag);
        info->tag_type = eTagOgg;
    }
}


/************************************************************************************
 *
 * extract picture from ogg vorbis file
 *
 ***********************************************************************************/
void extractPictureOggVorbis(int pic_fd, const char *path, int fd, long length)
{
    LOGI("looking for pictures in file \"%s\"\n", path);
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::Ogg::Vorbis::File f(stream);

    audioFileInfo info;
    info.pic_type = 0;

    TagLib::Ogg::XiphComment *tag = f.tag();
    if (tag != nullptr)
    {
        const TagLib::List<TagLib::FLAC::Picture*>& picList = tag->pictureList();
        handlePictureFlac(&info, pic_fd, picList);
    }
}


/************************************************************************************
 *
 * extract picture from ogg opus file
 *
 ***********************************************************************************/
void extractPictureOggOpus(int pic_fd, const char *path, int fd, long length)
{
    LOGI("looking for pictures in file \"%s\"\n", path);
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::Ogg::Opus::File f(stream);

    audioFileInfo info;
    info.pic_type = 0;

    TagLib::Ogg::XiphComment *tag = f.tag();
    if (tag != nullptr)
    {
        const TagLib::List<TagLib::FLAC::Picture*>& picList = tag->pictureList();
        handlePictureFlac(&info, pic_fd, picList);
    }
}
