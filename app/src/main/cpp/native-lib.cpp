#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-macro-usage"
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-goto"
/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>

#include "jni-utils.h"
#include "native-lib.h"
#include "native-cmd.h"
#include "native-lib-utils.h"

#include <ogg/flac/oggflacfile.h>
#include <ogg/vorbis/vorbisfile.h>
#include <flac/flacfile.h>

#include "native-mp3.h"
#include "native-mp4.h"
#include "native-flac.h"
#include "native-ogg.h"

#define JNIFN(TYPE, FN) extern "C" JNIEXPORT TYPE JNICALL Java_de_kromke_andreas_safmediascanner_NativeTaggerInterface_ ## FN


/************************************************************************************
 *
 * properties helper
 *
 ***********************************************************************************/
void handleProps(audioFileInfo *info, TagLib::AudioProperties *props)
{
    if (props != nullptr)
    {
        info->duration = props->lengthInMilliseconds();
        LOGD("   %-40s :  %d ms\n", "length", info->duration);
        LOGD("   %-40s :  %d kb/s\n", "bit rate", props->bitrate());
        LOGD("   %-40s :  %d Hz\n", "sample rate", props->sampleRate());
        LOGD("   %-40s :  %d\n", "channels", props->channels());
    }
}


/************************************************************************************
 *
 * combine Apple movement tags to one, i.e. precede movement number
 *
 ***********************************************************************************/
static const char * combineMovementString
(
    const char *movementNumber,
    const char *movementName
)
{
    if (movementName == nullptr)
    {
        return nullptr;
    }

    int mnum = -1;
    if (movementNumber != nullptr)
    {
        mnum = atoi(movementNumber); // NOLINT(cert-err34-c)
    }

    if ((mnum <= 0) || (mnum > 99999))
    {
        // empty or invalid movement number string
        return copyNonEmptyString(movementName);
    }

    char *combined = (char *) malloc(strlen(movementName) + strlen("99999. ") + 1);
    sprintf(combined, "%d. %s", mnum, movementName);
    return combined;
}


/************************************************************************************
 *
 * evaluate file info from raw tags
 *
 * note that these attribute are preset:
 *
 *  title as filename without extension
 *  album as directory name
 *  duration
 *  path
 *
 ***********************************************************************************/
static void initFileInfoFromTagsAndStat
(
    audioFileInfo *info,
    const char *path,
    long lastModified
)
{
    int m;
    int res;
    const char *s;

    info->path = path;

    //
    // get file modification time
    //

    if (lastModified != 0)
    {
        info->mtime = lastModified;
    }
    else
    {
        struct stat statbuf; // NOLINT(cppcoreguidelines-pro-type-member-init,hicpp-member-init)
        res = stat(info->path, &statbuf);
        if (res == 0)
        {
            info->mtime = statbuf.st_mtim.tv_sec;
        }
        else
        {
            LOGE("   cannot stat:  \"%s\"\n", info->path);
            info->mtime = 0;
        }
    }

    //
    // combine track and disc number to save some sort order work
    // for mp4: numerical values have already been set
    //

    if ((info->track_no == 0) && (info->no_tracks == 0))
    {
        s = info->tags[idTrack];
        res = evalNfromM(&info->track_no, &info->no_tracks, s);
        if (res != 0)
        {
            LOGW("   malformed n/m string for track:  \"%s\"\n", s);
        }
    }

    if ((info->disc_no == 0) && (info->no_discs == 0))
    {
        s = info->tags[idDiscNo];
        res = evalNfromM(&info->disc_no, &info->no_discs, s);
        if (res != 0)
        {
            LOGW("   malformed n/m string for disc:  \"%s\"\n", s);
        }
    }

    info->track_disc_no = 1000 * info->disc_no + info->track_no; // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)

    //
    // year is numerical, i.e. we ignore m
    //

    s = info->tags[idYear];
    res = evalNfromM(&info->year, &m, s);
    if ((res != 0) || (m != 0))
    {
        LOGW("   malformed n string for year:  \"%s\"\n", s);
    }

    //
    // In case of missing album use the directory name
    //

    s = info->tags[idAlbum];
    if (s != nullptr)
    {
        info->album = s;
    }

    //
    // just copy:
    //  performer ("artist")
    //  album performer ("album artist")
    //  subtitle
    //  composer
    //  conductor
    //

    info->performer = info->tags[idArtist];
    info->album_artist = info->tags[idAlbumArtist];
    info->subtitle = info->tags[idSubtitle];
    info->composer = info->tags[idComposer];
    info->conductor = info->tags[idConductor];

    //
    // prefer Apple Movement tag to standard tag
    //

    info->tags[idCombinedMovement] = combineMovementString(info->tags[idAppleMovementNo], info->tags[idAppleMovement]);
    s = info->tags[idCombinedMovement];
    if (s != nullptr)
    {
        info->title = s;
    }
    else
    {
        s = info->tags[idTitle];
        if (s != nullptr)
        {
            info->title = s;
        }
    }

    //
    // prefer Apple Work tag to standard tag
    //

    s = info->tags[idAppleWork];
    if (s != nullptr)
    {
        info->grouping = s;
    }
    else
    {
        info->grouping = info->tags[idGrouping];
    }

    //
    // repair strange genres (iTunes bug?)
    //

    s = info->tags[idGenre];
    if (s != nullptr)
    {
        if (!strcmp(s, "(32)"))
        {
            info->genre = "Classical";
        }
        else
        {
            info->genre = s;
        }
    }

    //
    // derive title from file name, if necessary
    //

    if ((info->title == nullptr) || (info->album == nullptr))
    {
        const char *fname = getFileNameFromPath(info->path);

        if (info->title == nullptr)
        {
            const char *dotpos = strrchr(fname, '.');
            info->tags[idTitle] = copyNonEmptyString(fname, dotpos);
            info->title = info->tags[idTitle];
        }

        //
        // derive album from directory name, if necessary
        //

        if (info->album == nullptr)
        {
            char dirname[MAXFNAME + 1];      // used in case of undefined album

            // get directory name as fallback solution for missing album information
            long lp = fname - path;
            const char *stp;
            extractDirNameFromPath(path, lp, dirname, &stp);
            //LOGI("opendir() directory name is \"%s\"\n", dirname);

            int cdnum = checkDirNameIsCdNumber(dirname, (int) strlen(dirname));
            if (cdnum > 0)
            {
                char super_dirname[MAXFNAME + 1];
                const char *sstp;
                extractDirNameFromPath(path, stp - path, super_dirname, &sstp);
                //LOGI("opendir() super directory name is \"%s\"\n", super_dirname);
                //LOGI("opendir() super directory is \"%.*s\"\n", (int) (stp - path), path);
            }
            info->tags[idAlbum] = copyNonEmptyString(dirname);
            info->album = info->tags[idAlbum];
        }
    }
}


/************************************************************************************
 *
 * deallocate dynamic memory
 *
 ***********************************************************************************/
static void freeTags(audioFileInfo *info)
{
    for (int i = 0; i < idNum; i++)
    {
        if (i == idAppleWork)
        {
            // skip derived tags
            continue;
        }

        if (info->tags[i] != nullptr)
        {
            free((void *) info->tags[i]);
            info->tags[i] = nullptr;
        }
    }
}


/************************************************************************************
 *
 * decide if file is an audio file
 *
 ***********************************************************************************/
static AudioFileType isAudioFile(const char *fname, const char **pos)
{
    if (fname[0] == '.')
    {
        return eInvalid;    // skip Apple files ._bla or similar
    }

    static const char *exts[] =
    {
        ".mp3",
        ".mp4",
        ".m4a",
        ".flac",
        ".ogg",
        ".opus"
    };

    *pos = strrchr(fname, '.');
    if (*pos != nullptr)
    {
        int i;
        for (i = 0; i < NUMELEMS(exts); i++)
        {
            if (!strcasecmp(*pos, exts[i]))
            {
                break;
            }
        }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "missing_default_case"
        switch(i) // NOLINT(hicpp-multiway-paths-covered)
        {
            case 0: return eMp3;
            case 1:
            case 2: return eMp4;
            case 3: return eFlac;
            case 4: return eOgg;
            case 5: return eOpus;
        }
#pragma clang diagnostic pop
    }

    return eInvalid;
}


/************************************************************************************
 *
 * file class for taglib
 *
 * extended for memory block mapped files (if fd is not seekable)
 *
 ***********************************************************************************/
class MyFileStream : public TagLib::IOStream
{
  private:

    const char *mPath;
    const char *mFileName;
    int mFileDescriptor;
    bool mbReadOnly;
    bool mbSeekable;
#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCUnusedGlobalDeclarationInspection"
    bool mbEndOfStream;
    int mLastError;
#pragma clang diagnostic pop
    char *mBuf;      // for non-seekable files
    long mLength;
    long mPos;

  public:

    // constructor, length 0 means "unknown"
    MyFileStream(const char *path, int fd, long length, bool bReadOnly)
    {
        mPath = path;
        mFileName = strrchr(mPath, '/');
        if (mFileName == nullptr)
        {
            mFileName = mPath;
        }
        else
        {
            mFileName++;
        }
        mFileDescriptor = fd;
        mbReadOnly = bReadOnly;
        mLength = (length > 0) ? length : -1;       // unknown
        clear();

        // check if file is seekable
        off_t res = lseek(mFileDescriptor, 0, SEEK_SET);
        if (res == 0)
        {
            mbSeekable = true;
            mBuf = nullptr;
        }
        else
        {
            int e = errno;
            if (e == ESPIPE)
            {
                LOGW("MyFileStream() : this file is not seekable because it is a pipe. Read it to memory.\n");
                e = readFileOfUnknownLength(mFileDescriptor, &mBuf, &mLength);
                if (e)
                {
                    LOGE("MyFileStream() : read failure with code %d (%s)\n", e, strerror(e));
                }
                else
                {
                    //LOGD("MyFileStream() : file read, length is %ld\n", mLength);
                }
            }
            else
            {
                LOGE("MyFileStream() : seek failure with code %d (%s)\n", e, strerror(e));
                mBuf = nullptr;
                mLength = 0;
            }
            mbSeekable = false;
        }

        mPos = 0;
    }

    // destructor
    virtual ~MyFileStream()
    {
        if (mBuf != nullptr)
        {
            free(mBuf);
            mBuf = nullptr;
        }
    }

    // get name
    virtual TagLib::FileName name() const
    {
        return mFileName;
    }

    // read
    virtual TagLib::ByteVector readBlock(unsigned long length)
    {
        //LOGD("readBlock(%ld)\n", length);
        char *buf = (char *) malloc(length);
        long n = 0;
        if (buf != nullptr)
        {
            if (mbSeekable)
            {
                // read from file
                n = read(mFileDescriptor, buf, length);
            }
            else
            {
                // read from memory
                if (length > mLength - mPos)
                {
                    LOGW("readBlock() : trying to read out of bounds\n");
                    n = mLength - mPos;
                }
                else
                {
                    n = length;
                }
                memcpy(buf, mBuf + mPos, (size_t) n);
                mPos += n;
            }

            //LOGD("readBlock() : %ld bytes read\n", n);
            if (n < length)
            {
                mbEndOfStream = true;
            }
        }
        TagLib::ByteVector temp(buf, (unsigned int) n);
        return temp;
    }

    // write
    virtual void writeBlock(const TagLib::ByteVector &data)
    {
        if (mbReadOnly)
        {
            LOGE("writeBlock() : trying to write to read-only file\n");
        }
        else
        if (mbSeekable)
        {
            const char *buf = data.data();
            unsigned int size = data.size();
            long n = write(mFileDescriptor, buf, size);
            if (n < size)
            {
                LOGE("writeBlock() : could not write all\n");
                mbEndOfStream = true;
            }
        }
        else
        {
            LOGE("writeBlock() : not implemented for non-seekable files\n");
        }
    }

    // insert block into existing file, making it larger
    virtual void insert(const TagLib::ByteVector &data,
                        unsigned long start = 0, unsigned long replace = 0)
    {
        LOGE("insert() : not implemented\n");
    }

    // remove block from existing file, making it smaller
    virtual void removeBlock(unsigned long start = 0, unsigned long length = 0)
    {
        LOGE("removeBlock() : not implemented\n");
    }

    virtual bool readOnly() const
    {
        return mbReadOnly;
    }

    virtual bool isOpen() const
    {
        return (mFileDescriptor >= 0);
    }

    virtual void seek(long offset, Position p = Beginning)
    {
        if (mbSeekable)
        {
            int whence;
            switch (p)
            {
                case Beginning:
                    whence = SEEK_SET;
                    break;
                case Current:
                    whence = SEEK_CUR;
                    break;
                case End:
                    whence = SEEK_END;
                    break;
                default:
                    LOGE("seek() : invalid mode\n");
                    return;
            }

            // return value of type off_t is ignored
            //LOGD("seek(pos=%ld, mode=%d)\n", offset, p);
            off_t res = lseek(mFileDescriptor, offset, whence);
            if (res == -1)
            {
                int e = errno;
                LOGE("seek() : failure with code %d (%s)\n", e, strerror(e));
                if (e == ESPIPE)
                {
                    LOGE("seek() : this file is not seekable because it is a pipe\n");
                }
            }
        }
        else
        {
            //LOGD("seek(pos=%ld, mode=%d)\n", offset, p);
            switch (p)
            {
                case Beginning:
                    break;
                case Current:
                    offset += mPos;
                    break;
                case End:
                    offset += mLength;
                    break;
                default:
                    LOGE("seek() : invalid mode\n");
                    return;
            }

            //LOGD("seek() : absolute position is %ld\n", offset);
            if (offset < 0)
            {
                LOGW("seek() : file position out of bounds\n");
                offset = 0;
            }
            else
            if (offset > mLength)
            {
                LOGW("seek() : file position out of bounds\n");
                offset = mLength;
            }
            mPos = offset;
        }
    }

    // clear errors, if any
    virtual void clear()
    {
        mbEndOfStream = false;
        mLastError = 0;
    }

    // get read/write pointer position
    virtual long tell() const
    {
        LOGD("tell()\n");
        long result;
        if (mbSeekable)
        {
            result = (long) lseek(mFileDescriptor, 0, SEEK_CUR);
        }
        else
        {
            result = mPos;
        }
        //LOGD("tell() => %ld\n", result);
        return result;
    }

    // get file length
    virtual long length()
    {
        if (mLength < 0)
        {
            off_t temp = lseek(mFileDescriptor, 0, SEEK_CUR);   // save
            mLength = lseek(mFileDescriptor, 0, SEEK_END);      // get end position
            (void) lseek(mFileDescriptor, temp, SEEK_SET);      // restore
        }
        //LOGD("length() => %ld\n", mLength);
        return mLength;
    }

    virtual void truncate(long length)
    {
        LOGE("truncate() : not implemented\n");
    }
};


/******************************************************************************
 *
 * glue from Taglib to file descriptor
 *
 *****************************************************************************/
TagLib::IOStream *createIOStream(const char *path, int fd, long length, bool bReadOnly)
{
    return new MyFileStream(path, fd, length, bReadOnly);
}


/************************************************************************************
 *
 * open audio file and extract folder.png or folder.jpg
 *
 ***********************************************************************************/
void extractPictureFromAudioFile(int pic_fd, const char *path, int fd, long length)
{
    LOGI("looking for pictures in file \"%s\"\n", path);
    const char *dotpos;
    AudioFileType audioType = isAudioFile(path, &dotpos);
    switch(audioType)
    {
        case eMp3:  extractPictureMp3(pic_fd, path, fd, length); break;
        case eMp4:  extractPictureMp4(pic_fd, path, fd, length); break;
        case eFlac: extractPictureFlac(pic_fd, path, fd, length); break;
        case eOpus: extractPictureOggOpus(pic_fd, path, fd, length); break;
        case eOgg:  extractPictureOggVorbis(pic_fd, path, fd, length); break;
        default:
            break;
    }
}


/******************************************************************************
 *
 * JNI function query table
 *
 *****************************************************************************/
void dbgPrintAudioFileInfo(const audioFileInfo *info)
{
    int diskNo = info->track_disc_no / 1000;
    int trackNo = info->track_disc_no % 1000;
    LOGD("track = %d\n", trackNo);
    if (diskNo > 0)
    {
        LOGD("disk = %d\n", diskNo);
    }
    LOGD("title = %s\n", info->title);
    LOGD("album = %s\n", info->album);
    LOGD("duration = %d\n", info->duration);
    LOGD("grouping = %s\n", info->grouping);
    LOGD("subtitle = %s\n", info->subtitle);
    LOGD("composer = %s\n", info->composer);
    LOGD("performer = %s\n", info->performer);
    LOGD("album_artist = %s\n", info->album_artist);
    LOGD("conductor = %s\n", info->conductor);
    LOGD("genre = %s\n", info->genre);
    LOGD("year = %d\n", info->year);
    LOGD("path = %s\n", info->path);

    // intermediates
    LOGD("track_no = %d\n", info->track_no);
    LOGD("disc_no = %d\n", info->disc_no);

    // raw text tags
    //const char *tags[idNum];    // raw

    LOGD("tagType = %d\n", info->tag_type);
}


/******************************************************************************
 *
 * JNI function query table
 *
 * -> https://www3.ntu.edu.sg/home/ehchua/programming/java/JavaNativeInterface.html#zz-5.1
 *
 *****************************************************************************/
JNIFN(jint, getTagsJNI)
(
    JNIEnv* env,
    jobject thisObj,
    jstring jfilepath,
    jlong length,
    jlong lastModified,
    jint command,
    jint param1,
    jint param2
)
{
    int rc = -1;
    const char *path = env->GetStringUTFChars(jfilepath, nullptr);
    LOGI("start handling file \"%s\" (length %d) with command %d\n", path, (int) length, command);
    audioFileInfo info;
    memset(&info, 0, sizeof(info));

    switch (command)
    {
        // It turned out that the file cannot be opened via "/proc/<procId>/fd/<filedesc>".
        // The file exists but is not readable.
        case cmdJniGetTagsFromPath:
        {
            FILE *f = fopen(path, "rb");
            if (f != nullptr)
            {
                char buf[256];
                int n = fread(buf, 1, 256, f);
                LOGI("read %d bytes\n", n);
                fclose(f);
                rc = 0;
            }
            else
            {
                LOGE("cannot open\n");
                rc = -2;
            }
        }
        break;

        case cmdJniGetTagsFromFd:
        {
            int fd = (int) param1;
            long fileLen = (long) length;

            // we could get a FILE this way:
            //            FILE *f = fdopen(param1, "rb");
            const char *dotpos;
            AudioFileType audioType = isAudioFile(path, &dotpos);
            if (audioType != eInvalid)
            {
                switch (audioType)
                {
                    case eMp3:
                        handleMp3(&info, path, fd, fileLen);
                        break;
                    case eMp4:
                        handleMp4(&info, path, fd, fileLen);
                        break;
                    case eFlac:
                        handleFlac(&info, path, fd, fileLen);
                        break;
                    case eOgg:
                        handleOggVorbis(&info, path, fd, fileLen);
                        break;
                    case eOpus:
                        handleOggOpus(&info, path, fd, fileLen);
                        break;
                    default:
                        handleMp3(&info, path, fd, fileLen);        // make lint happy...
                        break;
                }
                rc = 0;
            }
        }
        break;

        case cmdJniExtractPictureFromFdToFd:
        {
            int fd = (int) param1;
            int pic_fd = (int) param2;
            long fileLen = (long) length;
            extractPictureFromAudioFile(pic_fd, path, fd, fileLen);
            rc = 0;
        }
        break;

        default:
            LOGE("invalid command %d\n", command);
            rc = -1;
    }

    initFileInfoFromTagsAndStat(&info, path, (long) lastModified);
    dbgPrintAudioFileInfo(&info);

    //
    // copy result values to calling Java object
    //

    jniSetString(env, thisObj, "jniAlbum", info.album);
    jniSetString(env, thisObj, "jniGrouping", info.grouping);
    jniSetString(env, thisObj, "jniTitle", info.title);
    jniSetString(env, thisObj, "jniSubtitle", info.subtitle);
    jniSetString(env, thisObj, "jniComposer", info.composer);
    jniSetString(env, thisObj, "jniPerformer", info.performer);
    jniSetString(env, thisObj, "jniAlbumArtist", info.album_artist);
    jniSetString(env, thisObj, "jniConductor", info.conductor);
    jniSetString(env, thisObj, "jniGenre", info.genre);
    jniSetInt(env, thisObj, "jniYear", info.year);
    jniSetInt(env, thisObj, "jniDiskNo", info.disc_no);
    jniSetInt(env, thisObj, "jniTrackNo", info.track_no);
    jniSetInt(env, thisObj, "jniTotalDiskNo", info.no_discs);
    jniSetInt(env, thisObj, "jniTotalTrackNo", info.no_tracks);
    jniSetInt(env, thisObj, "jniDuration", info.duration);
    jniSetInt(env, thisObj, "jniTagType", info.tag_type);
    jniSetInt(env, thisObj, "jniPicType", info.pic_type);
    jniSetInt(env, thisObj, "jniPicSize", info.pic_size);

    freeTags(&info);
    env->ReleaseStringUTFChars(jfilepath, path);

    return rc;
}

#pragma clang diagnostic pop
