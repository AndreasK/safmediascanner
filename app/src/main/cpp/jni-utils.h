//
// Created by Andreas on 13.06.20.
//

#ifndef SAFMEDIASCANNER_JNI_UTILS_H
#define SAFMEDIASCANNER_JNI_UTILS_H

#include <jni.h>

extern void jniSetString(JNIEnv* env, jobject thisObj, const char *javaMemberName, const char *newValue);
extern void jniSetInt(JNIEnv* env, jobject thisObj, const char *javaMemberName, int newValue);

#endif //SAFMEDIASCANNER_JNI_UTILS_H

