/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>

#include "native-lib.h"
#include "native-flac.h"

// advanced taglib interface
#include <ogg/flac/oggflacfile.h>
#include <flac/flacfile.h>

#include "native-tag-utils.h"
#include "native-id3.h"
#include "native-xiph.h"


/************************************************************************************
 *
 * handle flac pictures
 *
 ***********************************************************************************/
void handlePictureFlac(audioFileInfo *info, int pic_fd, const TagLib::List<TagLib::FLAC::Picture*>& picList)
{
    auto size = picList.size();
    bool bDone = false;

    for (unsigned i = 0; i < size; i++)
    {
        TagLib::FLAC::Picture *pic = picList[i];
        int type = pic->type();
        TagLib::String str_mime = pic->mimeType();
        TagLib::ByteVector data = pic->data();
        auto picsize = data.size();

        const char *mime = str_mime.toCString(true /* unicode */);
        const char *stype;
        switch(type)
        {
            case TagLib::FLAC::Picture::FileIcon    : stype = "FileIcon   "; break;
            case TagLib::FLAC::Picture::FrontCover  : stype = "FrontCover "; break;
            case TagLib::FLAC::Picture::BackCover   : stype = "BackCover  "; break;
            case TagLib::FLAC::Picture::LeafletPage : stype = "LeafletPage"; break;
            case TagLib::FLAC::Picture::Media       : stype = "Media      "; break;
            case TagLib::FLAC::Picture::LeadArtist  : stype = "LeadArtist "; break;
            case TagLib::FLAC::Picture::Artist      : stype = "Artist     "; break;
            case TagLib::FLAC::Picture::Conductor   : stype = "Conductor  "; break;
            case TagLib::FLAC::Picture::Band        : stype = "Band       "; break;
            case TagLib::FLAC::Picture::Composer    : stype = "Composer   "; break;
            case TagLib::FLAC::Picture::Lyricist    : stype = "Lyricist   "; break;
            default: stype = "other";
        }
        LOGI("picture found in flac file, type = %s (%d), mime type = %s, size = %d x %d px\n", stype, type, mime, pic->width(), pic->height());

        if (picsize == 0)
        {
            LOGW("    skipping empty picture in file \"%s\"\n", info->path);
        }
        else
        if (!bDone)
        {
            bDone = handleIfCoverPicture(info, pic_fd, type, picsize, TagLib::FLAC::Picture::FrontCover, mime, &data);
        }
    }
}


/************************************************************************************
 *
 * advanced flac handling
 *
 ***********************************************************************************/
void handleFlac(audioFileInfo *info, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::FLAC::File f(stream, nullptr);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    if (f.hasXiphComment())
    {
        TagLib::Ogg::XiphComment *tag = f.xiphComment();
        if (tag != nullptr)
        {
            handleOgg(info, tag);
            info->tag_type = eTagOgg;
        }
    }
    if (f.hasID3v1Tag())
    {
        TagLib::ID3v1::Tag *tag = f.ID3v1Tag();
        if (tag != nullptr)
        {
            handleId3v1(info,  tag);
            info->tag_type = eTagId3v1;
        }
    }
    if (f.hasID3v2Tag())
    {
        TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
        if (tag != nullptr)
        {
            handleId3v2(info, tag);
            info->tag_type = eTagId3v2;
        }
    }

    if (info->pic_type == 0)
    {
        // no picture found so far. Search for one, but do not extract it
        const TagLib::List<TagLib::FLAC::Picture*>& picList = f.pictureList();
        handlePictureFlac(info, -1, picList);
    }
}


/************************************************************************************
 *
 * extract picture from flac file
 *
 ***********************************************************************************/
void extractPictureFlac(int pic_fd, const char *path, int fd, long length)
{
    LOGI("looking for pictures in file \"%s\"\n", path);
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::FLAC::File f(stream, nullptr);

    audioFileInfo info;
    info.pic_type = 0;

    if (f.hasXiphComment())
    {
        TagLib::Ogg::XiphComment *tag = f.xiphComment();
        if (tag != nullptr)
        {
            const TagLib::List<TagLib::FLAC::Picture*>& picList = tag->pictureList();
            handlePictureFlac(&info, pic_fd, picList);
        }
    }
    if (f.hasID3v1Tag())
    {
        /*
        TagLib::ID3v1::Tag *tag = f.ID3v1Tag();
        if (tag != nullptr)
        {

        }
        */
    }
    if (f.hasID3v2Tag())
    {
        TagLib::ID3v2::Tag *tag = f.ID3v2Tag();
        if (tag != nullptr)
        {
            extractPictureId3v2(&info, pic_fd, tag);
        }
    }

    if (info.pic_type == 0)
    {
        // no picture found so far. Search for one and extract it
        const TagLib::List<TagLib::FLAC::Picture*>& picList = f.pictureList();
        handlePictureFlac(&info, pic_fd, picList);
    }
}
