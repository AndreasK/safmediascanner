/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>
#include <assert.h>

// Android
#include <android/log.h>

#include "native-lib.h"
#include "taglib/toolkit/tbytevector.h"

#include "native-lib-utils.h"
#include "native-tag-utils.h"


/************************************************************************************
 *
 * write cover picture
 *
 * pic_type is 1 (jpeg) or 2 (png)
 *
 * called from:
 *  getMp4Tag()
 *  getMp3TagWithMultipleValues() -> handleIfCoverPicture()
 *
 ***********************************************************************************/
int writePicture(int pic_fd, TagLib::ByteVector *data)
{
    assert(pic_fd >= 0);
    size_t size = data->size();
    ssize_t written = write(pic_fd, data->data(), size);
    if (written < size)
    {
        LOGE("    file write failed on fd %d (errno %d)\n", pic_fd, errno);
        return -1;
    }
    else
    {
        return 0;
    }
}


/************************************************************************************
 *
 * write picture, if it is a cover picture and if it is jpg or png
 *
 * called by:
 *  getMp3TagWithMultipleValues()
 *  handlePictureFlac()
 *
 ***********************************************************************************/
bool handleIfCoverPicture
(
    audioFileInfo *info,
    int pic_fd,         // -1: do not extract
    int type,
    int picsize,
    int covertype,
    const char *mime,
    TagLib::ByteVector *data
)
{
    info->pic_type = 0;
    if (type == covertype)
    {
        LOGD("    front cover\n");
        if (!strcmp(mime, "image/jpeg"))
        {
            info->pic_type = 1;
            info->pic_size = picsize;
        }
        else if (!strcmp(mime, "image/png"))
        {
            info->pic_type = 2;
            info->pic_size = picsize;
        }
        else
        {
            LOGW("    skipping unsupported picture format in file \"%s\"\n", info->path);
        }
    }

    if ((info->pic_type != 0) && (pic_fd >= 0))
    {
        writePicture(pic_fd, data);
        return true;
    }
    else
    {
        return false;
    }
}


