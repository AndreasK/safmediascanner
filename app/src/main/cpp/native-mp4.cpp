#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-macro-usage"
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-goto"
/*
 * Copyright (C) 2020 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

//#include <string>
#include <dirent.h>
#include <cerrno>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <cinttypes>
#include <stdio.h>

// Android
#include <android/log.h>

#include "native-lib.h"
#include "native-mp4.h"
#include "native-cmd.h"

// advanced taglib interface
#include <mp4/mp4file.h>

#include "native-lib-utils.h"
#include "native-tag-utils.h"


/************************************************************************************
 *
 * mp4 helper
 *
 ***********************************************************************************/
static void getMp4Tag
(
    audioFileInfo *info,
    int pic_fd,                 // -1: do not extract
    const TagLib::MP4::Tag *tag,
    const char *key,
    const char *keyname,
    int tagno
)
{
    (void) keyname;             // for debugging purposes
    if (tag->contains(key))
    {
        TagLib::MP4::Item item = tag->item(key);
        if (!strcmp(key, "covr"))
        {
            // item list
            TagLib::MP4::CoverArtList sl = item.toCoverArtList();
            int n = sl.size();
            for (int i = 0; i < n; i++)
            {
                TagLib::MP4::CoverArt coverArt = sl[i];
                TagLib::MP4::CoverArt::Format format = coverArt.format();
                TagLib::ByteVector data = coverArt.data();
                LOGD("   data size is %d\n", data.size());
                if (data.size() == 0)
                {
                    LOGW("    skipping empty picture in file \"%s\"\n", info->path);
                }
                else
                {
                    int pic_type = 0;

                    if (format == TagLib::MP4::CoverArt::JPEG)
                    {
                        LOGD("   picture format is is JPEG\n");
                        if (info != nullptr)
                        {
                            info->pic_type = 1;
                            info->pic_size = data.size();
                        }
                        pic_type = 1;
                    }
                    else if (format == TagLib::MP4::CoverArt::PNG)
                    {
                        LOGD("   picture format is is PNG\n");
                        if (info != nullptr)
                        {
                            info->pic_type = 2;
                            info->pic_size = data.size();
                        }
                        pic_type = 2;
                    }
                    else
                    {
                        LOGW("   picture format is is %d (unsupported, neither jpeg nor png)\n", format);
                    }

                    if ((pic_type != 0) && (pic_fd >= 0))
                    {
                        LOGE("   writePicture() is not yet supported\n");
                        writePicture(pic_fd, &data);
                        return;
                    }
                }
            }
        }
        else
        if (!strcmp(key, "trkn"))
        {
            TagLib::MP4::Item::IntPair pair = item.toIntPair();
            info->track_no = pair.first;
            info->no_tracks = pair.second;
            LOGD("   %-40s is %d/%d\n", keyname, pair.first, pair.second);
        }
        else
        if (!strcmp(key, "disk"))
        {
            TagLib::MP4::Item::IntPair pair = item.toIntPair();
            info->disc_no = pair.first;
            info->no_discs = pair.second;
            LOGD("   %-40s is %d/%d\n", keyname, pair.first, pair.second);
        }
        else
        {
            // string list
            TagLib::StringList sl = item.toStringList();
            int n = sl.size();
            for (int i = 0; i < n; i++)
            {
                TagLib::String cs = sl[i];
                const char *s = cs.toCString(true /* unicode */);
                LOGD("   %-40s is \"%s\"\n", keyname, s);
                if ((i == 0) && (tagno >= 0))
                {
                    info->tags[tagno] = copyNonEmptyString(s);
                }
            }
        }
    }
}


/************************************************************************************
 *
 * advanced mp4 handling
 *
 ***********************************************************************************/
void handleMp4(audioFileInfo *info, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::MP4::File f(stream);
    TagLib::AudioProperties *props = f.audioProperties();
    handleProps(info, props);

    TagLib::MP4::Tag *tag = f.tag();
    if (tag != nullptr)
    {
        getMp4Tag(info, -1, tag, "\251alb",                         "album", idAlbum);
        getMp4Tag(info, -1, tag, "\251ART",                         "artist (i.e. performer)", idArtist);
        getMp4Tag(info, -1, tag, "aART",                            "album artist (i.e. album performer)", idAlbumArtist);
        getMp4Tag(info, -1, tag, "----:com.apple.iTunes:CONDUCTOR", "conductor", idConductor);
        getMp4Tag(info, -1, tag, "\251grp",                         "grouping (i.e. work title)", idGrouping);
        getMp4Tag(info, -1, tag, "\251nam",                         "title (i.e. movement title)", idTitle);
        getMp4Tag(info, -1, tag, "----:com.apple.iTunes:SUBTITLE",  "subtitle (e.g. opus number)", idSubtitle);
        getMp4Tag(info, -1, tag, "\251wrt",                         "composer", idComposer);
        getMp4Tag(info, -1, tag, "trkn",                            "track number / number of tracks", idTrack);
        getMp4Tag(info, -1, tag, "disk",                            "disk number / number of disks", idDiscNo);
        getMp4Tag(info, -1, tag, "\251gen",                         "genre", idGenre);
        getMp4Tag(info, -1, tag, "\251cmt",                         "comment", idComment);
        getMp4Tag(info, -1, tag, "ORIGINALDATE",                    "original year", -1);
        getMp4Tag(info, -1, tag, "\251day",                         "year", idYear);
        getMp4Tag(info, -1, tag, "\251mvn",                         "Apple movement", idAppleMovement);
        getMp4Tag(info, -1, tag, "\251mvi",                         "Apple movement number", idAppleMovementNo);
        getMp4Tag(info, -1, tag, "\251mvc",                         "Apple number of movements", idAppleMovementTotal);
        getMp4Tag(info, -1, tag, "\251wrk",                         "Apple work", idAppleMp4Work);
        getMp4Tag(info, -1, tag, "covr",                            "pictures", -1);

        // derived
        info->tags[idAppleWork] = info->tags[idAppleMp4Work];
    }

    info->tag_type = eTagMp4;
}


/************************************************************************************
 *
 * extract picture from mp4 file
 *
 ***********************************************************************************/
void extractPictureMp4(int pic_fd, const char *path, int fd, long length)
{
    TagLib::IOStream *stream = createIOStream(path, fd, length, true);
    TagLib::MP4::File f(stream);

    TagLib::MP4::Tag *tag = f.tag();
    if (tag != nullptr)
    {
        audioFileInfo info;
        getMp4Tag(&info, pic_fd, tag, "covr", "pictures", -1);
    }
}


#pragma clang diagnostic pop
